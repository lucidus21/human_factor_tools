인간공학실험 (406.305A_001), 2018 가을
======================================

*Team 1*: 강유진 (산업공학 17) 신범준 (산업공학 11) 이용재 (산업공학 17) 이재원 (산업공학 17) 정호진 (산업공학 17)

## dual_task_and_work_load

![demo](etc/img/all.png)

### 사용법

```
cd dual_task_and_work_load
pip install -r requirements.txt

./run_primary.sh  # Run primary task
./run_secondary.sh push fixed
./run_secondary.sh voice fixed
./run_secondary.sh push random
./run_secondary.sh voice random
```

- Primary Task: wpm 을 이용한 타자 측정
- Secondary Task: 음성 질문과 푸쉬 알림

### 각 스크립트 파일 안내

- `secondary_task/create_voices.py`: 주어진 텍스트를 합성된 음성파일(mp3)로 만들어내는 스크립트
- `secondary_task/secondary_task.py`: 여러 타입(음성 질문과 푸쉬 알림)과 여러 스타일(랜덤 간격과 고정 간격)으로 방해를 수행하는 스크립트
- `sentences/new_random_sentence.py`: 7+-2 단어로 구성된 랜덤 문장 생성기
- `sentences/rs_300.json`: `sentences/new_random_sentence.py`으로 생성된 문장들(wpm에 맞는 포맷으로 가공됨)

### secondary_task.py 사용법

```
python secondary_task.py \
    --file_path ./keep_track_task.json \  # 질문지
    --deliver_type ${deliver_type} \  # 음성 질문/푸쉬 알림
    --interval_style ${interval_style} \  # 고정 간격/랜덤 간격
    --interval_secs 50 \  # 방해 간격과 관련된 파라미터1
    --default_slack 5 \  # 방해 간격과 관련된 파라미터2
    --max_slack 20 \  # 방해 간격과 관련된 파라미터3
    --between_quotes_secs 3  # 방해 간격과 관련된 파라미터4
```
