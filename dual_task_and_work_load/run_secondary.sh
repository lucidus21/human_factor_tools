#!/bin/bash
set -eu

deliver_type=${1:-push}  # push/voice
interval_style=${2:-fixed}  # fixed/random


# 8분
pushd secondary_task
python secondary_task.py \
    --file_path ./keep_track_task.json \
    --deliver_type ${deliver_type} \
    --interval_style ${interval_style} \
    --interval_secs 50 \
    --default_slack 5 \
    --max_slack 20 \
    --between_quotes_secs 3
