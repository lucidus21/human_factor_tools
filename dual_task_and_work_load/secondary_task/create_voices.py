from pathlib import Path
import argparse
import hashlib
import json

import requests

VOICE_DIR = "voices/"


def generate_unique_sentences(keep_tracks):
    sentences = set()
    for keep_track in keep_tracks:
        for msg in keep_track["messages"]:
            sentences.add(msg)
        sentences.add(keep_track["query"])
    return list(sentences)


def main(args):
    url = "https://ttsmp3.com/makemp3.php"

    keep_tracks = json.load(open(args.file_path, "r"))
    sentences = generate_unique_sentences(keep_tracks)
    Path(VOICE_DIR).mkdir(exist_ok=True)

    for sentence in sentences:
        hashed = hashlib.md5(sentence.encode("utf-8")).hexdigest()
        hashed_mp3 = Path(hashed).with_suffix(".mp3")
        hashed_txt = Path(hashed).with_suffix(".txt")

        if hashed_mp3.exists() and hashed_txt.exists():
            print(f"[Alread Exists] Input: {sentence} Filename: {hashed_mp3}")
            continue

        payload = f"msg={sentence}&lang=Seoyeon&source=ttsmp3".encode("utf-8")
        headers = {
            'origin': "https://ttsmp3.com",
            'user-agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36",
            'content-type': "application/x-www-form-urlencoded",
            'accept': "*/*",
            'referer': "https://ttsmp3.com/text-to-speech/Korean/",
            'accept-encoding': "gzip, deflate, br",
            'accept-language': "en-US,en;q=0.9,ko;q=0.8",
            'cookie': "_ga=GA1.2.141510433.1538224002; _gid=GA1.2.713970947.1538224002",
            'cache-control': "no-cache",
            'postman-token': "ac8ef5e4-d8ee-6a7a-4d49-f781cc1ac3be"
        }

        resp1 = requests.request("POST", url, data=payload, headers=headers)
        resp1_dic = json.loads(resp1.text)
        mp3_url = resp1_dic["URL"].replace("\\", "")
        resp2 = requests.get(mp3_url, allow_redirects=True)

        mp3_path = Path(f'{VOICE_DIR}') / Path(hashed_mp3)
        txt_path = Path(f'{VOICE_DIR}') / Path(hashed_txt)

        print(f"Input: '{sentence}' mp3_path: {mp3_path} Output: {resp1.text}")

        with open(mp3_path, 'wb') as f:
            f.write(resp2.content)

        with open(txt_path, "w") as f:
            f.write(sentence)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--file_path', default="keep_track_task.json", type=str)
    args = parser.parse_args()
    main(args)
