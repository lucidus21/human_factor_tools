from pathlib import Path
import argparse
import hashlib
import random
import json
import time

from playsound import playsound
from tqdm import tqdm
import pandas as pd
import requests

from create_voices import VOICE_DIR


url = "https://hooks.slack.com/services/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"


def push(text):
    data = {"text": text}
    json_string = json.dumps(data)
    requests.post(url, headers={"Content-type": "application/json"}, data=json_string)
    print(f"Push: {text}")


def voice(text):
    hashed_mp3 = Path("voices") / Path(hashlib.md5(text.encode("utf-8")).hexdigest()).with_suffix(".mp3")
    playsound(str(hashed_mp3))
    print(f"Voice: {hashed_mp3}")


def sleep_progress(sleep_time, desc, n=100):
    if sleep_time == 0:
        return
    for _ in tqdm(range(n), desc=desc):
        time.sleep(sleep_time / n)


def main(args):
    random.seed(args.seed)

    track_tasks = json.load(open(args.file_path, "r"))
    random.shuffle(track_tasks)

    # Backup data
    pd.DataFrame(track_tasks).to_csv("shuffled_track_tasks.csv")

    deliver_method = {
        "push": push,
        "voice": voice,
    }
    deliver = deliver_method[args.deliver_type]

    for n, track_task in enumerate(track_tasks, start=1):
        print("<Messages>")
        st = time.time()
        slack = random.randint(0, args.max_slack) if args.interval_style == "random" else args.default_slack
        sleep_progress(slack, desc=f"Slack Interval({slack:.2f}s)")

        for message in track_task["messages"]:
            deliver(message)
            time.sleep(args.between_quotes_secs)
        print("<Question>")
        deliver(track_task["query"])
        deliver_time = time.time() - st

        if deliver_time > args.interval_secs:
            print("[WARNING] Please give more interval_secs & less max_slack")

        print(f"<Summary>")
        print(f"deliver_type:{args.deliver_type} deliver_time:{deliver_time:.0f}s lag: {track_task['lag']}")
        print(f"<Answer>: {track_task['answer']}")
        wait_secs = max(args.interval_secs - deliver_time, 0)
        sleep_progress(wait_secs, desc=f"Wait Interval({wait_secs:.2f}s)")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--file_path', required=True, type=str)
    parser.add_argument('--seed', default=0, type=int)

    parser.add_argument('--deliver_type', default="push", type=str,
                        choices=["push", "voice"])
    parser.add_argument('--interval_style', default="fixed", type=str,
                        choices=["fixed", "random"])
    parser.add_argument('--interval_secs', default=30, type=int)
    parser.add_argument('--default_slack', default=0, type=int)
    parser.add_argument('--max_slack', default=10, type=int)
    parser.add_argument('--between_quotes_secs', default=5, type=int,
                        help="For voice, it needs at least 5")
    args = parser.parse_args()
    main(args)
