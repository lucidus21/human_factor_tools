#!/bin/bash
set -eu

user=${1:-user}
stats=${2:-./stats}


wpm --load-json sentences/rs_300.json \
    --tag ${user} \
    --stats-file ${user}-${stats}

pushd secondary_task
python secondary_task.py \
    --file_path ./keep_track_task.json \
    --deliver_type push \
    --interval_style fixed \
    --interval_style fixed \
