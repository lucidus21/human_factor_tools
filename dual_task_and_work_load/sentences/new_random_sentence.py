import json
import argparse

from tqdm import tqdm
import requests


def main(num):
    url = "http://watchout4snakes.com/wo4snakes/Random/NewRandomSentence"

    headers = {
        'accept': "*/*",
        'origin': "http://watchout4snakes.com",
        'x-requested-with': "XMLHttpRequest",
        'user-agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36",
        'referer': "http://watchout4snakes.com/wo4snakes/random/randomsentence",
        'accept-encoding': "gzip, deflate",
        'accept-language': "en-US,en;q=0.9,ko;q=0.8",
        'cookie': "__utmc=132193160; __utma=132193160.1362550193.1538188487.1538188487.1538198664.2; __utmz=132193160.1538198664.2.2.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); __utmt=1; __utmb=132193160.5.10.1538198664",
        'cache-control': "no-cache",
        'postman-token': "94edbf8a-d1bc-7bcb-7d4c-d2abf5de5332"
        }


    quotes = []
    cnt = 0
    total = 0
    while cnt < num:
        response = requests.request("POST", url, headers=headers)
        text = response.text
        word_counts = len(text.split())
        total += 1
        # Miller's magic number
        if 5 <= word_counts <= 9:
            quote = {
                "id": cnt,
                "title": f"Random Sentence {cnt}",
                "text": text,
                "author": "Beomjun Shin"
            }
            quotes.append(quote)
            cnt += 1
            print(f"cnt:{cnt} ratio: {cnt/total} content:{quote}")

    json.dump(quotes, open(f"rs_{num}.json", "w"), indent=4)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--num', default=10, type=int)
    args = parser.parse_args()
    main(args.num)
